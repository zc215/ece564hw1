//
//  DataModel.swift
//  564HW1
//
//  Created by Brandon C. on 2023/9/4.
//

import Foundation

struct DukePerson: CustomStringConvertible {
    let DUID: Int
    var fName: String
    var lName: String
    var email: String
    var from: String
    var gender: Gender
    var role: Role
    
    var description: String{
        var subject: String
        // var possessive: String
        var object: String
        
        switch gender {
        case .Male:
            subject = "He"
            //possessive = "His"
            object = "him"
        case .Female:
            subject = "She"
            //possessive = "Her"
            object = "her"
        case .Unknown, .Other:
            subject = "They"
            //possessive = "Their"
            object = "them"
        }
        return "\(fName) \(lName) is a \(role). \(subject) is from \(from). You can reach \(object) at \(email)."
    }
    
    init(DUID: Int, fName: String = "firstname", lName: String = "lastname", email: String = "unknown", from: String = "unknown", gender: Gender = .Unknown, role: Role = .Unknown) {
            self.DUID = DUID
            self.fName = fName
            self.lName = lName
            self.email = email
            self.from = from
            self.gender = gender
            self.role = role
        }
}

class DataModel {
    var database: [DukePerson] = []

    func add(_ newPerson: DukePerson) -> Bool {
        if newPerson.DUID == 0 || database.contains(where: { $0.DUID == newPerson.DUID }) {
            return false
        }
        database.append(newPerson)
        return true
    }
    
    func update(_ updatedPerson: DukePerson) -> Bool {
        if let index = database.firstIndex(where: { $0.DUID == updatedPerson.DUID }) {
            database[index] = updatedPerson
            return true
        }
        database.append(updatedPerson)
        return false
    }
    
    func delete(_ DUID: Int) -> Bool {
        if let index = database.firstIndex(where: { $0.DUID == DUID }) {
            database.remove(at: index)
            return true
        }
        return false
    }

    func find(_ DUID: Int) -> DukePerson? {
        return database.first { $0.DUID == DUID }
    }
    
    func find(lastName lName: String, firstName fName: String = "*") -> [DukePerson]? {
        if lName == "*" && fName == "*" {
            return database
        }
        
        let filteredPersons = database.filter { person in
            (lName == "*" || person.lName.lowercased() == lName.lowercased()) &&
            (fName == "*" || person.fName.lowercased() == fName.lowercased())
        }
        
        if filteredPersons.isEmpty {
            return nil
        }
        return filteredPersons
    }
    
    func list() -> String {
        var listString = ""
        for (index, person) in database.enumerated() {
            let entry = "\(index + 1). \(person.fName) \(person.lName)\n(\(person.email)) - \(person.DUID)"
            listString += entry

            if index < database.count - 1 {
                listString += "\n\n" // Add two newlines to separate entries
            }
        }
        return listString
    }
}
