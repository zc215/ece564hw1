//
//  DataStructures.swift
//  564HW1
//
//  Created by Brandon C. on 2023/9/4.
//

import Foundation

enum Role : String {
    case Unknown = "Unknown" // has not been specified
    case Professor = "Professor"
    case TA = "TA"
    case Student = "Student"
    case Other = "Other" // has been specified, but is not Professor, TA, or Student
    
    static func fromUserInput(_ userInput: String) -> Role {
        switch userInput.lowercased() {
        case "student":
            return .Student
        case "professor":
            return .Professor
        case "ta":
            return .TA
        case "other":
            return .Other
        default:
            return .Unknown
        }
    }
}

enum Gender : String {
    case Unknown = "Unknown" // has not been specified
    case Male = "Male"
    case Female = "Female"
    case Other = "Other" // has been specified, but is not “Male” or “Female"
    
    static func fromUserInput(_ userInput: String) -> Gender {
        switch userInput.lowercased() {
        case "male":
            return .Male
        case "female":
            return .Female
        case "other":
            return .Other
        default:
            return .Unknown
        }
    }
}
