//
//  ViewController.swift
//  564HW1
//
//  Created by Brandon C. on 2023/9/4.
//

import UIKit

class ViewController: UIViewController {
    let dataModel = DataModel()
    let scrollView = UIScrollView()
    let messageView = UITextView()
    let imageView = UIImageView(image: UIImage(named: "sunset.jpg"))
    let inputField = UITextField()
    let secondInputField = UITextField()
    let addButton = UIButton()
    let deleteButton = UIButton()
    let updateButton = UIButton()
    let findButton = UIButton()
    let listButton = UIButton()
    let helpButton = UIButton()
    let clearButton = UIButton()
    let outputView = UITextView()
    
    /*  Other options
    let mySegmented = UISegmentedControl()
    let mySwitch = UISwitch()
    let myStepper = UIStepper()
    let mySlider = UISlider()
    let myPicker = UIPickerView()
    */
    override func viewDidLoad() {
        let view = self.view!
        view.backgroundColor = .white

        imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        imageView.alpha = 0.8
        view.addSubview(imageView)

        messageView.frame = CGRect(x: 40, y: 55, width:400, height: 50)
        messageView.font = UIFont(name: "Courier", size: 25.0)
        messageView.backgroundColor = .clear
        messageView.alpha = 0.8
        messageView.text = "Ready for input"
        view.addSubview(messageView)

        inputField.frame = CGRect(x: 40, y: 225, width:180, height: 50)
        inputField.font = UIFont(name: "Courier", size: 30.0)
        inputField.backgroundColor = .yellow
        inputField.alpha = 0.8
        view.addSubview(inputField)
        inputField.placeholder = "DUID"

        secondInputField.frame = CGRect(x: 40, y: 305, width:300, height: 50)
        secondInputField.font = UIFont(name: "Courier", size: 30.0)
        secondInputField.backgroundColor = .yellow
        secondInputField.alpha = 0.8
        view.addSubview(secondInputField)
        secondInputField.placeholder = "parameters"

        addButton.frame = CGRect(x: 40, y: 100, width:125, height: 40)
        addButton.backgroundColor = .blue
        addButton.layer.cornerRadius = 10
        addButton.titleLabel?.numberOfLines = 0
        addButton.titleLabel?.textAlignment = .center
        addButton.titleLabel?.lineBreakMode = .byWordWrapping
        addButton.setTitle("Add", for: UIControl.State())
        addButton.titleLabel?.font = UIFont(name: "Courier", size: CGFloat(25))
        addButton.setTitleColor(UIColor.black, for: .highlighted)
        addButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        view.addSubview(addButton)

        deleteButton.frame = CGRect(x: 215, y: 100, width:125, height: 40)
        deleteButton.backgroundColor = .blue
        deleteButton.layer.cornerRadius = 10
        deleteButton.titleLabel?.numberOfLines = 0
        deleteButton.titleLabel?.textAlignment = .center
        deleteButton.titleLabel?.lineBreakMode = .byWordWrapping
        deleteButton.setTitle("Delete", for: UIControl.State())
        deleteButton.titleLabel?.font = UIFont(name: "Courier", size: CGFloat(25))
        deleteButton.setTitleColor(UIColor.black, for: .highlighted)
        deleteButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        view.addSubview(deleteButton)

        updateButton.frame = CGRect(x: 40, y: 155, width:125, height: 40)
        updateButton.backgroundColor = .blue
        updateButton.layer.cornerRadius = 10
        updateButton.titleLabel?.numberOfLines = 0
        updateButton.titleLabel?.textAlignment = .center
        updateButton.titleLabel?.lineBreakMode = .byWordWrapping
        updateButton.setTitle("Update", for: UIControl.State())
        updateButton.titleLabel?.font = UIFont(name: "Courier", size: CGFloat(25))
        updateButton.setTitleColor(UIColor.black, for: .highlighted)
        updateButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        view.addSubview(updateButton)

        findButton.frame = CGRect(x: 215, y: 155, width:125, height: 40)
        findButton.backgroundColor = .blue
        findButton.layer.cornerRadius = 10
        findButton.titleLabel?.numberOfLines = 0
        findButton.titleLabel?.textAlignment = .center
        findButton.titleLabel?.lineBreakMode = .byWordWrapping
        findButton.setTitle("Find", for: UIControl.State())
        findButton.titleLabel?.font = UIFont(name: "Courier", size: CGFloat(25))
        findButton.setTitleColor(UIColor.black, for: .highlighted)
        findButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        view.addSubview(findButton)

        listButton.frame = CGRect(x: 30, y: 380, width:160, height: 40)
        let ovalPath = UIBezierPath(roundedRect: listButton.bounds, cornerRadius: listButton.bounds.height / 2)
        let maskLayer = CAShapeLayer()
        maskLayer.path = ovalPath.cgPath
        listButton.layer.mask = maskLayer
        listButton.backgroundColor = .blue
        listButton.layer.cornerRadius = 10
        listButton.titleLabel?.numberOfLines = 0
        listButton.titleLabel?.textAlignment = .center
        listButton.titleLabel?.lineBreakMode = .byWordWrapping
        listButton.setTitle("List", for: UIControl.State())
        listButton.titleLabel?.font = UIFont(name: "Courier", size: CGFloat(25))
        listButton.setTitleColor(UIColor.black, for: .highlighted)
        listButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        view.addSubview(listButton)

        helpButton.frame = CGRect(x: 205, y: 380, width:160, height: 40)
        let seccnodOvalPath = UIBezierPath(roundedRect: helpButton.bounds, cornerRadius: helpButton.bounds.height / 2)
        let secondMaskLayer = CAShapeLayer()
        secondMaskLayer.path = seccnodOvalPath.cgPath
        helpButton.layer.mask = secondMaskLayer
        helpButton.backgroundColor = .blue
        helpButton.layer.cornerRadius = 10
        helpButton.titleLabel?.numberOfLines = 0
        helpButton.titleLabel?.textAlignment = .center
        helpButton.titleLabel?.lineBreakMode = .byWordWrapping
        helpButton.setTitle("Help", for: UIControl.State())
        helpButton.titleLabel?.font = UIFont(name: "Courier", size: CGFloat(25))
        helpButton.setTitleColor(UIColor.black, for: .highlighted)
        helpButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        view.addSubview(helpButton)

        clearButton.frame = CGRect(x: 260, y: 210, width: 80, height: 80) // Make the button square
        clearButton.backgroundColor = .blue
        clearButton.layer.cornerRadius = clearButton.frame.size.width / 2 // Set corner radius to half of the width
        clearButton.setTitle("Clear", for: .normal)
        clearButton.titleLabel?.font = UIFont(name: "Courier", size: CGFloat(25))
        clearButton.setTitleColor(UIColor.black, for: .highlighted)
        clearButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        view.addSubview(clearButton)

        outputView.frame = CGRect(x: 40, y: 440, width:310, height: 390)
        outputView.backgroundColor = .yellow
        outputView.alpha = 0.8
        view.addSubview(outputView)
    }

    
    @objc func buttonPressed(_ sender: UIButton) {
        var outputViewInfo = ""
        var messageViewInfo = ""
        var duid = 0
        
        outputViewInfo = outputView.text
        
        let textString = sender.titleLabel?.text ?? ""
                
        if let str = inputField.text, let temp = Int(str) {
            duid = temp
        }
        
        // Parse the secondInputField string
        let tokens = secondInputField.text?.components(separatedBy: ",") ?? []
        var parsedData: [String: String] = [:]

        for token in tokens {
            let keyValue = token.components(separatedBy: "=")
            if keyValue.count == 2 {
                let key = keyValue[0].trimmingCharacters(in: .whitespaces).lowercased()
                let value = keyValue[1].trimmingCharacters(in: .whitespaces)
                parsedData[key] = value
            }
        }

        let newPerson = DukePerson(
            DUID: duid,
            fName: parsedData["fn"] ?? "Firstname",
            lName: parsedData["ln"] ?? "Lastname",
            email: parsedData["em"] ?? "unknown",
            from: parsedData["fr"] ?? "unknown",
            gender: Gender(rawValue: parsedData["ge"] ?? "Unknown") ?? Gender.fromUserInput(parsedData["ge"] ?? "Unknown"),
            role: Role(rawValue: parsedData["ro"] ?? "Unknown") ?? Role.fromUserInput(parsedData["ro"] ?? "Unknown")
//            gender: Gender.fromUserInput(parsedData["ge"] ?? "Unknown"),
//            role: Role.fromUserInput(parsedData["ro"] ?? "Unknown")
        )

        switch textString {
        case "Add":
            outputViewInfo = ""
            if dataModel.add(newPerson) {
                messageViewInfo += "Added \(newPerson.fName) \(newPerson.lName)"
                outputViewInfo += "\(newPerson.description)\n"
            } else {
                messageViewInfo += "Invalid DUID\n"
            }
        case "Delete":
            outputViewInfo = ""
            if dataModel.delete(duid) {
                messageViewInfo += "Deleted Person \(duid)"
            } else {
                messageViewInfo += "Delete Failure."
            }
        case "Update":
            outputViewInfo = ""
            if var foundPerson = dataModel.find(duid) {
                foundPerson.fName = newPerson.fName == "Firstname" ? foundPerson.fName : newPerson.fName
                foundPerson.lName = newPerson.lName == "Lastname" ? foundPerson.lName : newPerson.lName
                foundPerson.email = newPerson.email == "unknown" ? foundPerson.email : newPerson.email
                foundPerson.from = newPerson.from == "unknown" ? foundPerson.from : newPerson.from
                foundPerson.gender = newPerson.gender == .Unknown ? foundPerson.gender : newPerson.gender
                foundPerson.role = newPerson.role == .Unknown ? foundPerson.role : newPerson.role
                if dataModel.update(foundPerson) {
                    messageViewInfo += "Updated \(foundPerson.fName) \(foundPerson.lName)"
                    outputViewInfo += "\(foundPerson.description)\n"
                }
            } else {
                messageViewInfo += "Person not found"
            }
        case "Find":
            outputViewInfo = ""
            if let foundPerson = dataModel.find(duid) {
                messageViewInfo = "Person found: \(foundPerson.fName) \(foundPerson.lName)"
                outputViewInfo += foundPerson.description
                break
            }
            
            var foundPersons: [DukePerson]? = nil
            if newPerson.fName == "Firstname" {
                foundPersons = dataModel.find(lastName: newPerson.lName)
            } else {
                foundPersons = dataModel.find(lastName: newPerson.lName, firstName: newPerson.fName)
            }
            
            if foundPersons == nil {
                outputViewInfo += "\nPerson not found.\n"
            } else {
                messageViewInfo += "\(foundPersons!.count) person(s) found"
                for person in foundPersons! {
                    outputViewInfo += "\n\(person.description)\n"
                }
            }
        case "List":
            outputViewInfo = ""
            messageViewInfo += "Successful"
            let personList = dataModel.list()
            outputViewInfo += "List of Persons:\n\(personList)\n"
        case "Help":
            outputViewInfo = ""
            outputViewInfo += "Valid set of parameters, separated by comma:\n"
            outputViewInfo += "  - fn = first name\n"
            outputViewInfo += "  - ln = last name\n"
            outputViewInfo += "  - em = email\n"
            outputViewInfo += "  - fr = where they are from\n"
            outputViewInfo += "  - ge = gender - Male, Female, Other, Unknown\n"
            outputViewInfo += "  - ro = role - Professor, TA, Student, Other, Unknown\n"
            
            outputViewInfo += "\nExample Syntax of the parameters:\n"
            outputViewInfo += "  - fn=Ric, ln=Telford, em=ric.telford@duke.edu, fr=Chatham County, ge=Male, ro=Professor\n"
            
            outputViewInfo += "\nWhat input is required and [optional] for each button:\n"
            outputViewInfo += "  - add DUID [fn = first name, ln = last name, ...]\n"
            outputViewInfo += "  - update DUID [fn = first name, ln= last name, ...]\n"
            outputViewInfo += "  - find DUID\n"
            outputViewInfo += "  - find ln = last name [fn = first name]\n"
            outputViewInfo += "  - list\n"
            outputViewInfo += "  - help\n"
        case "Clear":
            outputViewInfo = ""
            messageViewInfo = ""
            inputField.text = ""
            secondInputField.text = ""
        default:
            outputViewInfo += "\nInvalid operation"
        }

        secondInputField.text = ""
        outputView.text = outputViewInfo
        messageView.text = messageViewInfo
    }
}
